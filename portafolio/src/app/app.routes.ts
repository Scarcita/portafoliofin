import { RouterModule, Routes } from "@angular/router";
import { InicioComponent } from "./components/inicio/inicio.component";



const APP_ROUTES: Routes = [
  { path: 'home', component: InicioComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'navbar' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);

